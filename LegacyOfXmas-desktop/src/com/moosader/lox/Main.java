package com.moosader.lox;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

public class Main {
	public static void main(String[] args) {
		LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration();
		cfg.title = "LegacyOfXmas";
		cfg.useGL20 = false;
		cfg.width = 480 * 2;
		cfg.height = 320 * 2;
		
		LegacyOfXmas application = new LegacyOfXmas();
		new LwjglApplication(application, cfg);
	}
}
