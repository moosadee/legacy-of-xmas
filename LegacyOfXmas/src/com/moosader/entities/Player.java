package com.moosader.entities;

import java.util.ArrayList;
import java.util.Random;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.moosader.entityproperties.Animation;
import com.moosader.managers.MapManager;
import com.moosader.maps.Tile;
import com.moosader.utilities.GlobalOptions;

public class Player extends CharacterEntity {
	
	protected Weapon m_weapon;
	protected float m_actionTimeout;
	protected final float m_actionTimeoutMax = 10.0f;
	
	public static final float PlayerMovementSpeed = 2.0f; // TODO: this is haxxy
	
	public Player(Texture texture, Vector2 dimensions) {
		m_weapon = new Weapon(texture, dimensions);
		initializeShit();
		setupSprite(texture, dimensions);
		updateFrame();
	}
	
	public void initializeShit() {
		super.initializeShit();
		m_movementSpeed = Player.PlayerMovementSpeed; 
		m_action = Animation.COL_WALK;
		m_stats.ATK = 20;
		m_objName = "Player";
		m_coord.x = 140;
		m_coord.y = 140;
	}
	
	@Override
	public void handleDamage(int amount) {
		super.handleDamage(amount);
		updateAction(Animation.COL_HURT);
		m_actionTimeout = m_actionTimeoutMax;
	}
	
	public Sprite getAccessorySprite() {
		if ( m_actionTimeout > 0 && m_animation.getAction() == Animation.COL_ATTACK ) {
			return m_weapon.getSprite();
		}
		return null;
	}
	
	public boolean isAttacking() {
		return ( m_actionTimeout > 0 && m_animation.getAction() == Animation.COL_ATTACK );
	}
	
	public void update() {
		super.update();
		if ( m_actionTimeout > 0 ) {
			m_actionTimeout -= 1.0f;
			m_weapon.update();
		}
		else {
			// Cancel attack, back to walking.
			updateAction(Animation.COL_WALK);
		}
		handlePlayerInput();
	}
	
	@Override
	public void updateFrame() {
		super.updateFrame();
	}
	
	public void updateAction(int action) {
		m_action = action;
		m_animation.setAction(m_action);
		updateFrame();		
	}
	
	public Weapon getWeapon() {
		return m_weapon;
	}

	public void handlePlayerInput() {
		// MOVEMENT
		if ( !MapManager.isTransitioning() ) {
			if ( Gdx.input.isKeyPressed(Keys.W )) {
				m_action = Animation.COL_WALK;
				moveUp();
			} 
			else if ( Gdx.input.isKeyPressed(Keys.S)) {	
				m_action = Animation.COL_WALK;
				moveDown();
			}
			else if ( Gdx.input.isKeyPressed(Keys.A)) {
				m_action = Animation.COL_WALK;
				moveLeft();
			}
			else if ( Gdx.input.isKeyPressed(Keys.D)) {
				m_action = Animation.COL_WALK;
				moveRight();
			}
			// ATTACKING
			if ( Gdx.input.isKeyPressed( Keys.SPACE ) ) {
				attack();
			}
			
			// Debug
			if ( Gdx.input.isKeyPressed( Keys.F4 ) ) {
				GlobalOptions.debugOut( "\nPlayer coordinates: " + m_coord.x + ", " + m_coord.y + ",\t" + 
						"Player dimensions: " + m_dimen.x + ", " + m_dimen.y );
			}
			if ( Gdx.input.isKeyPressed( Keys.F9 ) ) {
				Random poop = new Random();
				int x = poop.nextInt(320);
				int y = poop.nextInt(320);
				setPosition(new Vector2(x, y));
			}
		}
	}
	
	protected void attack() {
		if ( m_actionTimeout <= 0 && m_tileMoveCounter <= 0 ) {
			m_actionTimeout = m_actionTimeoutMax;
			updateAction(Animation.COL_ATTACK);
			m_weapon.setupWeapon(m_coord, m_animation.getDirection());
		}
	}
}

