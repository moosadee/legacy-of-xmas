package com.moosader.entities;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.moosader.entityproperties.Animation;
import com.moosader.managers.TextureManager;
import com.moosader.utilities.GlobalOptions;

public class Npc extends CharacterEntity {
	public static final int NPC_SNOWHARE = 0;
	public static final int NPC_PENGUIN = 1;

	public Npc() {
		m_movementSpeed = 0.8f;
		m_stats.ATK = 10;
		m_objName = "Npc";
	}
	
	public void setup(String npcType) {
		if ( npcType.equals( "snowhare" ) ) {
			setupSnowHare();
		}
		else if ( npcType.equals( "penguin" )) {
			setupPenguin();
		}
		else
		{
			GlobalOptions.debugOut( "Could not set up entity, \"" + npcType + "\"." );
		}
	}
	
	protected void setupSnowHare() {
		setupSprite( TextureManager.character_snowhare, new Vector2( 20, 20 ) );
		m_objName += "Snowhare";
		m_stats.ATK = 0;
	}
	
	protected void setupPenguin() {
		setupSprite( TextureManager.character_penguin, new Vector2( 20, 20 ) );
		m_objName += "Penguin";
	}	
}
