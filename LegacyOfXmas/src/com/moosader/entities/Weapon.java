package com.moosader.entities;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;
import com.moosader.entityproperties.Animation;
import com.moosader.entityproperties.Direction;
import com.moosader.entityproperties.Stats;

public class Weapon extends IBaseObject {

	protected boolean m_isActive;
	protected Animation m_animation;
	
	public Weapon(Texture texture, Vector2 dimensions) {
		initializeShit();
		setupSprite(texture, dimensions);
	}
	
	public void initializeShit() {
		super.initializeShit();
		m_animation = new Animation(m_dimen);
		m_animation.setAction(Animation.COL_WEAPON);
	}
	
	void setActive() {
		if ( !m_isActive ) {
			m_isActive = true;
		}
	}
	void setInactive() {
		m_isActive = false;
	}
	
	public void update() {
		m_animation.update();
		updateFrame();
	}
	
	public void setupWeapon(Vector2 playerCoords, int playerDirection) {
		m_animation.setDirection(playerDirection);
		m_animation.setAction(Animation.COL_WEAPON);
		m_animation.setAnimateSpeed(1.0f);
		m_animation.setLoopAnimation(false);
		m_animation.resetFrame();
		m_coord = new Vector2( playerCoords );
		
		if ( playerDirection == Animation.ROW_SOUTH ) {
			m_coord.y -= m_dimen.y;
		}
		else if ( playerDirection == Animation.ROW_NORTH ) {
			m_coord.y += m_dimen.y;
		}
		else if ( playerDirection == Animation.ROW_EAST ) {
			m_coord.x += m_dimen.x;
		}
		else if ( playerDirection == Animation.ROW_WEST ) {
			m_coord.x -= m_dimen.x;
		}

		updateFrame();
	}
	
	@Override
	public void updateFrame() {		
		m_region.setRegion(	
				(int)m_animation.getTextureCoordinates().x, 
				(int)m_animation.getTextureCoordinates().y,
				(int)m_dimen.x, (int)m_dimen.y);
		
		m_sprite.setRegion(m_region);
		m_sprite.setPosition( m_coord.x, m_coord.y );
	}
}
