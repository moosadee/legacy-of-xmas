package com.moosader.utilities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;

public class GlobalOptions {
	public static boolean DebugMode = false;
	
	public static void checkInput() {
		if ( Gdx.input.isKeyPressed( Keys.F5 ) ) {
			DebugMode = true;
		}
		if ( Gdx.input.isKeyPressed( Keys.F6 ) ) {
			DebugMode = false;
		}
	}
	
	public static void debugOut(String text) {
		if ( DebugMode ) { 
			System.out.println( text );
		}
	}
}
